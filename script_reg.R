library(sf)
library(mapsf)
library(readxl)
library(rmapshaper)

# Import municipalities and arrondissements (IGN)
com <- st_read("input/geom_ign/COMMUNE.shp", quiet = TRUE)
com <- com[,c("INSEE_COM", "NOM", "INSEE_REG", "POPULATION")]


# Import reference data (INSEE)
df <- data.frame(read_excel("input/data/base_cc_comparateur.xlsx", skip = 5, 
                                sheet = "COM"))
df <- df[,c("CODGEO", "SUPERF")]

# Join to the base map
com <- merge(com, df, by.x = "INSEE_COM", by.y = "CODGEO", all.x = TRUE)

# Project and delete oversea territories
com <- com[!com$INSEE_REG %in% c("01", "02", "03", "04", "05", "06"),]
com <- st_transform(com, 2154)

# Simplify geometries (Keep 10 % of points) / all communes are still present
com <- ms_simplify(com, keep = 0.1)

# Enrich with attributes
app <- data.frame(read_excel("input/aggregates/table-appartenance-geo-communes-23.xlsx",
                             skip = 5))
app <- app[,c("CODGEO", "DEP", "REG", "EPCI", "ZE2020", "UU2020", "BV2022")]

com <- merge(com, app, by.x = "INSEE_COM", by.y = "CODGEO", 
               all.x = TRUE)

# Colnames
com$INSEE_REG <- NULL
names(com)[3:4] <- c("POP_2020", "SUPERF_2023")

# Transform and round geom decimals
geom_round <- function(x, precision){
  st_geometry(x) %>% st_sfc(precision = precision) %>% 
  st_as_binary  %>% 
  st_as_sfc
}

com <- st_transform(com, 4326)
com$geometry <- geom_round(com, precision = 100000)
unique(st_is_valid(com)) # TRUE
com <- st_set_crs(com, "EPSG:4326")



# Ile-de-France
sel <- com[com$REG == "11",]
st_write(sel, "output_reg/idf_com.geojson")

# Auvergne Rhône-Alpes
sel <- com[com$REG == "84",]
st_write(sel, "output_reg/auv_rh_com.geojson")

# Hauts-de-France
sel <- com[com$REG == "32",]
st_write(sel, "output_reg/hdf_com.geojson")

# PACA 
sel <- com[com$REG == "93",]
st_write(sel, "output_reg/paca_com.geojson")

# Grand Est
sel <- com[com$REG == "44",]
st_write(sel, "output_reg/grand_est_com.geojson")

# Occitanie
sel <- com[com$REG == "76",]
st_write(sel, "output_reg/occitanie_com.geojson")

# Normandie
sel <- com[com$REG == "28",]
st_write(sel, "output_reg/norm_com.geojson")

# Nouvelle Aquitaine
sel <- com[com$REG == "75",]
st_write(sel, "output_reg/naqu_com.geojson")

# Centre-Val-de-Loire
sel <- com[com$REG == "24",]
st_write(sel, "output_reg/cvdl_com.geojson")

# Bourgogne-Franche-Comté
sel <- com[com$REG == "27",]
st_write(sel, "output_reg/bour_fct_com.geojson")

# Bretagne
sel <- com[com$REG == "53",]
st_write(sel, "output_reg/bzh_com.geojson")

# Corse
sel <- com[com$REG == "94",]
st_write(sel, "output_reg/cor_com.geojson")

# Pays-de-la-Loire
sel <- com[com$REG == "52",]
st_write(sel, "output_reg/pdl_com.geojson")


# DROM

# Import municipalities and arrondissements (IGN)
com <- st_read("input/geom_ign/COMMUNE.shp", quiet = TRUE)
com <- com[,c("INSEE_COM", "NOM", "INSEE_REG", "POPULATION")]


# Import reference data (INSEE)
df <- data.frame(read_excel("input/data/base_cc_comparateur.xlsx", skip = 5, 
                                sheet = "COM"))
df <- df[,c("CODGEO", "SUPERF")]

# Join to the base map
com <- merge(com, df, by.x = "INSEE_COM", by.y = "CODGEO", all.x = TRUE)

# Project and delete oversea territories
com <- com[com$INSEE_REG %in% c("01", "02", "03", "04", "05", "06"),]

# Simplify geometries (Keep 10 % of points) / all communes are still present
com <- ms_simplify(com, keep = 0.1)

# Enrich with attributes
app <- data.frame(read_excel("input/aggregates/table-appartenance-geo-communes-23.xlsx",
                             skip = 5))
app <- app[,c("CODGEO", "DEP", "REG", "EPCI", "ZE2020", "UU2020", "BV2022")]

com <- merge(com, app, by.x = "INSEE_COM", by.y = "CODGEO", 
               all.x = TRUE)

# Estimate missing values (Mayotte)
est <- com[is.na(com$SUPERF),]

est <- st_transform(est, 4471)
est$SUPERF <- as.numeric(st_area(est)/1000000)
est <- st_transform(est, 4326)

com <- com[!is.na(com$SUPERF),]
com <- rbind(com, est)

com$geometry <- geom_round(com, precision = 100000)
unique(st_is_valid(com)) # TRUE
com <- st_set_crs(com, "EPSG:4326")


# Colnames
com$INSEE_REG <- NULL
names(com)[3:4] <- c("POP_2020", "SUPERF_2023")

# Guadeloupe
sel <- com[com$REG == "01",]
st_write(sel, "output_reg/guad_com.geojson")

# Martinique
sel <- com[com$REG == "02",]
st_write(sel, "output_reg/mart_com.geojson")

# Guyane
sel <- com[com$REG == "03",]
st_write(sel, "output_reg/guy_com.geojson")

# La Réunion44
sel <- com[com$REG == "04",]
st_write(sel, "output_reg/reu_com.geojson")

# Mayotte
sel <- com[com$REG == "06",]
st_write(sel, "output_reg/may_com.geojson")